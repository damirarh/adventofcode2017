package day25a

data class Action(val value: Boolean, val delta: Int, val newState: String)
data class State(val falseAction: Action, val trueAction: Action)

fun main(args: Array<String>) {
    val sample = mutableMapOf(
            Pair("A", State(Action(true, 1, "B"), Action(false, -1, "B"))),
            Pair("B", State(Action(true, -1, "A"), Action(true, 1, "A")))
    )
    println(calculateChecksum(sample, 6))

    val input = mutableMapOf(
            Pair("A", State(Action(true, 1, "B"), Action(false, 1, "C"))),
            Pair("B", State(Action(false, -1, "A"), Action(false, 1, "D"))),
            Pair("C", State(Action(true, 1, "D"), Action(true, 1, "A"))),
            Pair("D", State(Action(true, -1, "E"), Action(false, -1, "D"))),
            Pair("E", State(Action(true, 1, "F"), Action(true, -1, "B"))),
            Pair("F", State(Action(true, 1, "A"), Action(true, 1, "E")))
    )
    println(calculateChecksum(input, 12368930))
}

fun calculateChecksum(states: Map<String, State>, steps: Int): Int {
    var position = 0
    val tape = mutableSetOf<Int>()
    var state = "A"
    for (step in 1..steps) {
        val action = if (tape.contains(position)) states[state]?.trueAction else states[state]?.falseAction
        if (action != null) {
            if (action.value) {
                tape.add(position)
            } else {
                tape.remove(position)
            }
            position += action.delta
            state = action.newState
        }
    }
    return tape.size
}