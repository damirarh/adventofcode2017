package day14b

fun main(args: Array<String>) {
    println(countRegions("flqrgnkx"))
    println(countRegions("hxtvlmkl"))
}

fun countRegions(keyString: String): Int {
    val grid = generateGrid(keyString)
    var groupCount = 0
    for (y in 0..127) {
        for (x in 0..127) {
            if (grid[y][x] == -1) {
                groupCount++
                markGroup(grid, groupCount, x, y)
            }
        }
    }
    return groupCount
}

fun markGroup(grid: Array<Array<Int>>, groupIndex: Int, x: Int, y: Int) {
    if (x in 0..127 && y in 0..127)
    if (grid[y][x] == -1) {
        grid[y][x] = groupIndex
        markGroup(grid, groupIndex, x + 1, y)
        markGroup(grid, groupIndex, x - 1, y)
        markGroup(grid, groupIndex, x, y + 1)
        markGroup(grid, groupIndex, x, y - 1)
    }
}

fun generateGrid(keyString: String) : Array<Array<Int>> {
    return (0..127)
            .map { computeHash("$keyString-$it")}
            .map { it.toCharArray().map { hexToArray(it) }.reduce { joined, next -> joined.plus(next) } }
            .toTypedArray()
}

fun hexToArray(hex: Char): Array<Int> {
    return when (hex) {
        '0' -> arrayOf( 0,  0,  0,  0)
        '1' -> arrayOf( 0,  0,  0, -1)
        '2' -> arrayOf( 0,  0, -1,  0)
        '3' -> arrayOf( 0,  0, -1, -1)
        '4' -> arrayOf( 0, -1,  0,  0)
        '5' -> arrayOf( 0, -1,  0, -1)
        '6' -> arrayOf( 0, -1, -1,  0)
        '7' -> arrayOf( 0, -1, -1, -1)
        '8' -> arrayOf(-1,  0,  0,  0)
        '9' -> arrayOf(-1,  0,  0, -1)
        'a' -> arrayOf(-1,  0, -1,  0)
        'b' -> arrayOf(-1,  0, -1, -1)
        'c' -> arrayOf(-1, -1,  0,  0)
        'd' -> arrayOf(-1, -1,  0, -1)
        'e' -> arrayOf(-1, -1, -1,  0)
        'f' -> arrayOf(-1, -1, -1, -1)
        else -> arrayOf(0,  0,  0,  0)
    }
}


fun computeHash(input: String): String {
    val bytes = input.toCharArray()
            .map { it.toByte().toInt() }
            .plus(arrayOf(17, 31, 73, 47, 23))
    val sparseHash = computeSparseHash(bytes)
    val denseHash = computeDenseHash(sparseHash)
    return denseHash.joinToString("") { it.toString(16).padStart(2,'0') }
}

fun computeDenseHash(bytes: IntArray): List<Int> {
    val result = mutableListOf<Int>()
    for (outer in 0..15) {
        var computed = 0
        for (inner in 0..15) {
            computed = computed xor bytes[outer * 16 + inner]
        }
        result.add(computed)
    }
    return result
}

fun computeSparseHash(lengths: List<Int>): IntArray {
    val list = (0 until 256).toList().toIntArray()
    var current = 0
    var skipSize = 0
    for (round in 1..64) {
        for (length in lengths) {
            reverse(list, current, length)
            current += length + skipSize
            skipSize++
        }
    }
    return list
}

fun reverse(list: IntArray, start: Int, length: Int) {
    if (length >= 2) {
        for (offset in 0..(length - 2) / 2) {
            swap(list, (start + offset) % list.size, (start + length - 1 - offset) % list.size)
        }
    }
}

fun swap(list: IntArray, first: Int, second: Int) {
    val temp = list[first]
    list[first] = list[second]
    list[second] = temp
}