package day17a

fun main(args: Array<String>) {
    println(calculateSpinlock(3, 2017))
    println(calculateSpinlock(363, 2017))
}

fun calculateSpinlock(steps: Int, insertions: Int): Int {
    val buffer = mutableListOf(0)
    var position = 0
    for (value in 1..insertions) {
        position = (position + steps) % buffer.size + 1
        buffer.add(position, value)
    }
    return buffer[(position + 1) % buffer.size]
}