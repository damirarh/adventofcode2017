package day22b

enum class Direction {
    UP, DOWN, LEFT, RIGHT
}
enum class State {
    WEAKENED, INFECTED, FLAGGED
}
data class Coords(val x: Int, val y: Int)

fun main(args: Array<String>) {
    val sample = arrayOf(
            "..#",
            "#..",
            "...")
    println(countInfections(sample, 100))
    println(countInfections(sample, 10000000))

    val input = arrayOf(
            "###.#######...#####.#..##",
            ".####...###.##...#..#....",
            ".#.#...####.###..##..##.#",
            "########.#.#...##.#.##.#.",
            "..#.#...##..#.#.##..####.",
            "..#.#.....#....#####..#..",
            "#.#..##...#....#.##...###",
            ".#.##########...#......#.",
            ".#...#..##...#...###.#...",
            "......#.###.#..#...#.####",
            ".#.###.##...###.###.###.#",
            ".##..##...#.#.#####.#...#",
            "#...#..###....#.##.......",
            "####.....######.#.##..#..",
            "..#...#..##.####.#####.##",
            "#...#.#.#.#.#...##..##.#.",
            "#####.#...#.#.#.#.##.####",
            "....###...#.##.#.##.####.",
            ".#....###.#####...#.....#",
            "#.....#....#####.#..#....",
            ".#####.#....#..##.#.#.###",
            "####.#..#..##..#.#..#.###",
            ".##.##.#.#.#.#.#..####.#.",
            "#####..##.#.#..#..#...#..",
            "#.#..#.###...##....###.##")
    println(countInfections(input, 10000000))
}

fun countInfections(input: Array<String>, bursts: Int): Int {
    val states = parse(input)
    var current = Coords(0, 0)
    var direction = Direction.UP
    var infections = 0

    for (burst in 1..bursts) {
        val state = states[current]
        if (state == State.WEAKENED) {
            states[current] = State.INFECTED
            infections++
        } else if (state == State.INFECTED) {
            direction = when (direction) {
                Direction.UP -> Direction.RIGHT
                Direction.RIGHT -> Direction.DOWN
                Direction.DOWN -> Direction.LEFT
                Direction.LEFT -> Direction.UP
            }
            states[current] = State.FLAGGED
        } else if (state == State.FLAGGED) {
            direction = when (direction) {
                Direction.UP -> Direction.DOWN
                Direction.DOWN -> Direction.UP
                Direction.RIGHT -> Direction.LEFT
                Direction.LEFT -> Direction.RIGHT
            }
            states.remove(current)
        }
        else { // clean
            direction = when (direction) {
                Direction.UP -> Direction.LEFT
                Direction.LEFT -> Direction.DOWN
                Direction.DOWN -> Direction.RIGHT
                Direction.RIGHT -> Direction.UP
            }
            states[current] = State.WEAKENED
        }
        current = when (direction) {
            Direction.UP -> Coords(current.x, current.y - 1)
            Direction.DOWN -> Coords(current.x, current.y + 1)
            Direction.LEFT -> Coords(current.x - 1, current.y)
            Direction.RIGHT -> Coords(current.x + 1, current.y)
        }
    }
    return infections
}

fun parse(input: Array<String>): MutableMap<Coords, State> {
    val states = mutableMapOf<Coords, State>()
    for (y in 0 until input.size) {
        for (x in 0 until input[0].length) {
            if (input[y][x] == '#') {
                states[Coords(x - input[0].length / 2, y - input.size / 2)] = State.INFECTED
            }
        }
    }
    return states
}