package day03a

fun main(args: Array<String>) {
    println(distance(1))
    println(distance(12))
    println(distance(23))
    println(distance(1024))
    println(distance(361527))
}

fun distance(address: Int): Int {
    if (address == 1) {
        return 0
    }
    val edgeLength = edgeLength(address)
    val positionOnEdge = (address - (edgeLength - 2) * (edgeLength - 2)) % (edgeLength - 1)
    return (edgeLength - 1) / 2 + Math.abs(positionOnEdge - (edgeLength - 1) / 2)
}

fun edgeLength(address: Int): Int {
    val sqrtCeil = Math.ceil(Math.sqrt(address.toDouble())).toInt()
    return sqrtCeil - (sqrtCeil % 2) + 1
}