package day22a

enum class Direction {
    UP, DOWN, LEFT, RIGHT
}
data class Coords(val x: Int, val y: Int)

fun main(args: Array<String>) {
    val sample = arrayOf(
            "..#",
            "#..",
            "...")
    println(countInfections(sample, 7))
    println(countInfections(sample, 70))
    println(countInfections(sample, 10000))

    val input = arrayOf(
            "###.#######...#####.#..##",
            ".####...###.##...#..#....",
            ".#.#...####.###..##..##.#",
            "########.#.#...##.#.##.#.",
            "..#.#...##..#.#.##..####.",
            "..#.#.....#....#####..#..",
            "#.#..##...#....#.##...###",
            ".#.##########...#......#.",
            ".#...#..##...#...###.#...",
            "......#.###.#..#...#.####",
            ".#.###.##...###.###.###.#",
            ".##..##...#.#.#####.#...#",
            "#...#..###....#.##.......",
            "####.....######.#.##..#..",
            "..#...#..##.####.#####.##",
            "#...#.#.#.#.#...##..##.#.",
            "#####.#...#.#.#.#.##.####",
            "....###...#.##.#.##.####.",
            ".#....###.#####...#.....#",
            "#.....#....#####.#..#....",
            ".#####.#....#..##.#.#.###",
            "####.#..#..##..#.#..#.###",
            ".##.##.#.#.#.#.#..####.#.",
            "#####..##.#.#..#..#...#..",
            "#.#..#.###...##....###.##")
    println(countInfections(input, 10000))
}

fun countInfections(input: Array<String>, bursts: Int): Int {
    val infected = parse(input)
    var current = Coords(0, 0)
    var direction = Direction.UP
    var infections = 0

    for (burst in 1..bursts) {
        if (infected.contains(current)) {
            infected.remove(current)
            direction = when (direction) {
                Direction.UP -> Direction.RIGHT
                Direction.RIGHT -> Direction.DOWN
                Direction.DOWN -> Direction.LEFT
                Direction.LEFT -> Direction.UP
            }
        } else {
            infections++
            infected.add(current)
            direction = when (direction) {
                Direction.UP -> Direction.LEFT
                Direction.LEFT -> Direction.DOWN
                Direction.DOWN -> Direction.RIGHT
                Direction.RIGHT -> Direction.UP
            }
        }
        current = when (direction) {
            Direction.UP -> Coords(current.x, current.y - 1)
            Direction.DOWN -> Coords(current.x, current.y + 1)
            Direction.LEFT -> Coords(current.x - 1, current.y)
            Direction.RIGHT -> Coords(current.x + 1, current.y)
        }
    }
    return infections
}

fun parse(input: Array<String>): MutableList<Coords> {
    val infected = mutableListOf<Coords>()
    for (y in 0 until input.size) {
        for (x in 0 until input[0].length) {
            if (input[y][x] == '#') {
                infected.add(Coords(x - input[0].length / 2, y - input.size / 2))
            }
        }
    }
    return infected
}