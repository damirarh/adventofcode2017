package day15a

data class Generator(val value: Long, val factor: Long)

fun main(args: Array<String>) {
    println(judge(5, 65, 8921))
    println(judge(40000000, 65, 8921))
    println(judge(40000000, 516, 190))
}

fun judge(sampleSize: Int, startA: Long, startB: Long): Int {
    var count = 0
    var generatorA = Generator(startA, 16807)
    var generatorB = Generator(startB, 48271)
    for (iteration in 1..sampleSize) {
        generatorA = generateNext(generatorA)
        generatorB = generateNext(generatorB)
        if (generatorA.value % 65536 == generatorB.value % 65536) {
            count++
        }
    }
    return count
}

fun generateNext(current: Generator): Generator {
    return Generator(current.value * current.factor % 2147483647, current.factor)
}