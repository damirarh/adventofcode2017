package day03b

data class Coords(val x: Int, val y: Int)

fun main(args: Array<String>) {
    val values = arrayListOf(1)
    while (values.last() < 361527) {
        values.add(neighborSum(values.count() + 1, values))
    }
    println(values.last())
}

fun neighborSum(address: Int, values: List<Int>): Int {
    val coords = toCoords(address)
    var sum = 0
    for (dx in -1..1) {
        for (dy in -1..1) {
            val neighborAddress = toAddress(Coords(coords.x + dx, coords.y + dy))
            sum += if (values.count() >= neighborAddress) values[neighborAddress - 1] else 0
        }
    }
    return sum
}

fun toCoords(address: Int): Coords {
    if (address == 1) {
        return Coords(0, 0)
    }
    val edgeLength = edgeLength(address)
    val offsetOnSquare = address - (edgeLength - 2) * (edgeLength - 2)
    val positionOnEdge = offsetOnSquare % (edgeLength - 1) - (edgeLength - 1) / 2
    val edgeIndex = offsetOnSquare / (edgeLength - 1)
    return when (edgeIndex) {
        0 -> Coords((edgeLength - 1) / 2, positionOnEdge)
        1 -> Coords(-positionOnEdge, (edgeLength - 1) / 2)
        2 -> Coords(-(edgeLength - 1) / 2, -positionOnEdge)
        3 -> Coords(positionOnEdge, -(edgeLength - 1) / 2)
        else -> Coords(-positionOnEdge, positionOnEdge)
    }
}

fun toAddress(coords: Coords) : Int {
    val maxAbsCoord = Math.max(Math.abs(coords.x), Math.abs(coords.y))
    if (maxAbsCoord == 0) {
        return 1
    }
    val innerSquareOffset = (2 * maxAbsCoord - 1) * (2 * maxAbsCoord - 1)
    return when {
        coords.y == -maxAbsCoord -> innerSquareOffset + 7 * maxAbsCoord + coords.x
        coords.x == -maxAbsCoord -> innerSquareOffset + 5 * maxAbsCoord - coords.y
        coords.y == maxAbsCoord -> innerSquareOffset + 3 * maxAbsCoord  - coords.x
        else -> innerSquareOffset + maxAbsCoord + coords.y
    }
}

fun edgeLength(address: Int): Int {
    val sqrtCeil = Math.ceil(Math.sqrt(address.toDouble())).toInt()
    return sqrtCeil - (sqrtCeil % 2) + 1
}
