package day23a

fun main(args: Array<String>) {
    val program = listOf(
            "set b 57",
            "set c b",
            "jnz a 2",
            "jnz 1 5",
            "mul b 100",
            "sub b -100000",
            "set c b",
            "sub c -17000",
            "set f 1",
            "set d 2",
            "set e 2",
            "set g d",
            "mul g e",
            "sub g b",
            "jnz g 2",
            "set f 0",
            "sub e -1",
            "set g e",
            "sub g b",
            "jnz g -8",
            "sub d -1",
            "set g d",
            "sub g b",
            "jnz g -13",
            "jnz f 2",
            "sub h -1",
            "set g b",
            "sub g c",
            "jnz g 2",
            "jnz 1 3",
            "sub b -17",
            "jnz 1 -23")
    println(execute(program))
}

fun execute(instructions: List<String>): Long {
    val registers = mutableMapOf<String, Long>()
    var mulCount = 0L
    var instructionPointer = 0L

    while (instructionPointer in 0 until instructions.size) {
        val tokens = instructions[instructionPointer.toInt()].split(" ")
        when (tokens[0]) {
            "set" -> registers[tokens[1]] = getValue(tokens[2], registers)
            "sub" -> registers[tokens[1]] = getValue(tokens[1], registers) - getValue(tokens[2], registers)
            "mul" -> {
                registers[tokens[1]] = getValue(tokens[1], registers) * getValue(tokens[2], registers)
                mulCount++
            }
            "jnz" -> if (getValue(tokens[1], registers) != 0L) instructionPointer += getValue(tokens[2], registers) - 1
        }
        instructionPointer++
    }
    return mulCount
}

fun getValue(registerOrLiteral: String, registers: Map<String, Long>): Long {
    return try {
        registerOrLiteral.toLong()
    } catch (e: NumberFormatException) {
        registers[registerOrLiteral] ?: 0
    }
}