package day10a

fun main(args: Array<String>) {
    val sample = arrayOf(3, 4, 1, 5)
    val sampleHash = computeHash(5, sample)
    println(sampleHash[0] * sampleHash[1])

    val input = arrayOf(199, 0, 255, 136, 174, 254, 227, 16, 51, 85, 1, 2, 22, 17, 7, 192)
    val hash = computeHash(256, input)
    println(hash[0] * hash[1])
}

fun computeHash(size: Int, lengths: Array<Int>): IntArray {
    val list = (0 until size).toList().toIntArray()
    var current = 0
    var skipSize = 0
    for (length in lengths) {
        reverse(list, current, length)
        current += length + skipSize
        skipSize++
    }
    return list
}

fun reverse(list: IntArray, start: Int, length: Int) {
    if (length >= 2) {
        for (offset in 0..(length - 2) / 2) {
            swap(list, (start + offset) % list.size, (start + length - 1 - offset) % list.size)
        }
    }
}

fun swap(list: IntArray, first: Int, second: Int) {
    val temp = list[first]
    list[first] = list[second]
    list[second] = temp
}