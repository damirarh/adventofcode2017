package day17b

fun main(args: Array<String>) {
    println(calculateSpinlock(3, 9))
    println(calculateSpinlock(363, 50000000))
}

fun calculateSpinlock(steps: Int, insertions: Int): Int {
    var shortCircuitValue = 0
    var position = 0
    var size = 1
    for (value in 1..insertions) {
        position = (position + steps) % size + 1
        size++
        if (position == 1) {
            shortCircuitValue = value
        }
    }
    return shortCircuitValue
}