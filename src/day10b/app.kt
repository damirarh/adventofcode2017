package day10b

fun main(args: Array<String>) {
    println(computeHash(""))
    println(computeHash("AoC 2017"))
    println(computeHash("1,2,3"))
    println(computeHash("1,2,4"))
    println(computeHash("199,0,255,136,174,254,227,16,51,85,1,2,22,17,7,192"))
}

fun computeHash(input: String): String {
    val bytes = input.toCharArray()
            .map { it.toByte().toInt() }
            .plus(arrayOf(17, 31, 73, 47, 23))
    val sparseHash = computeSparseHash(bytes)
    val denseHash = computeDenseHash(sparseHash)
    return denseHash.joinToString("") { it.toString(16).padStart(2,'0') }
}

fun computeDenseHash(bytes: IntArray): List<Int> {
    val result = mutableListOf<Int>()
    for (outer in 0..15) {
        var computed = 0
        for (inner in 0..15) {
            computed = computed xor bytes[outer * 16 + inner]
        }
        result.add(computed)
    }
    return result
}

fun computeSparseHash(lengths: List<Int>): IntArray {
    val list = (0 until 256).toList().toIntArray()
    var current = 0
    var skipSize = 0
    for (round in 1..64) {
        for (length in lengths) {
            reverse(list, current, length)
            current += length + skipSize
            skipSize++
        }
    }
    return list
}

fun reverse(list: IntArray, start: Int, length: Int) {
    if (length >= 2) {
        for (offset in 0..(length - 2) / 2) {
            swap(list, (start + offset) % list.size, (start + length - 1 - offset) % list.size)
        }
    }
}

fun swap(list: IntArray, first: Int, second: Int) {
    val temp = list[first]
    list[first] = list[second]
    list[second] = temp
}