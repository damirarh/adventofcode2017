package day18a

fun main(args: Array<String>) {
    val sampleProgram = listOf("set a 1",
            "add a 2",
            "mul a a",
            "mod a 5",
            "snd a",
            "set a 0",
            "rcv a",
            "jgz a -1",
            "set a 1",
            "jgz a -2")
    println(execute(sampleProgram))

    val program = listOf("set i 31",
            "set a 1",
            "mul p 17",
            "jgz p p",
            "mul a 2",
            "add i -1",
            "jgz i -2",
            "add a -1",
            "set i 127",
            "set p 735",
            "mul p 8505",
            "mod p a",
            "mul p 129749",
            "add p 12345",
            "mod p a",
            "set b p",
            "mod b 10000",
            "snd b",
            "add i -1",
            "jgz i -9",
            "jgz a 3",
            "rcv b",
            "jgz b -1",
            "set f 0",
            "set i 126",
            "rcv a",
            "rcv b",
            "set p a",
            "mul p -1",
            "add p b",
            "jgz p 4",
            "snd a",
            "set a b",
            "jgz 1 3",
            "snd b",
            "set f 1",
            "add i -1",
            "jgz i -11",
            "snd a",
            "jgz f -16",
            "jgz a -19")
    println(execute(program))
}

fun execute(instructions: List<String>): Long {
    val registers = mutableMapOf<String, Long>()
    var frequency = 0L
    var instructionPointer = 0L

    while (instructionPointer in 0 until instructions.size) {
        val tokens = instructions[instructionPointer.toInt()].split(" ")
        when (tokens[0]) {
            "snd" -> frequency = getValue(tokens[1], registers)
            "set" -> registers[tokens[1]] = getValue(tokens[2], registers)
            "add" -> registers[tokens[1]] = getValue(tokens[1], registers) + getValue(tokens[2], registers)
            "mul" -> registers[tokens[1]] = getValue(tokens[1], registers) * getValue(tokens[2], registers)
            "mod" -> registers[tokens[1]] = getValue(tokens[1], registers) % getValue(tokens[2], registers)
            "rcv" -> if (getValue(tokens[1], registers) != 0L) return frequency
            "jgz" -> if (getValue(tokens[1], registers) > 0) instructionPointer += getValue(tokens[2], registers) - 1
        }
        instructionPointer++
    }
    return 0
}

fun getValue(registerOrLiteral: String, registers: Map<String, Long>): Long {
    return try {
        registerOrLiteral.toLong()
    } catch (e: NumberFormatException) {
        registers[registerOrLiteral] ?: 0
    }
}