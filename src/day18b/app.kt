package day18b

import java.util.*

data class Program(var instructionPointer: Long, val registers: MutableMap<String, Long>, val queue: Queue<Long>, var sendCount: Long, var running: Boolean)

fun main(args: Array<String>) {
    val sampleProgram = listOf("snd 1",
            "snd 2",
            "snd p",
            "rcv a",
            "rcv b",
            "rcv c",
            "rcv d")
    println(execute(sampleProgram))

    val program = listOf("set i 31",
            "set a 1",
            "mul p 17",
            "jgz p p",
            "mul a 2",
            "add i -1",
            "jgz i -2",
            "add a -1",
            "set i 127",
            "set p 735",
            "mul p 8505",
            "mod p a",
            "mul p 129749",
            "add p 12345",
            "mod p a",
            "set b p",
            "mod b 10000",
            "snd b",
            "add i -1",
            "jgz i -9",
            "jgz a 3",
            "rcv b",
            "jgz b -1",
            "set f 0",
            "set i 126",
            "rcv a",
            "rcv b",
            "set p a",
            "mul p -1",
            "add p b",
            "jgz p 4",
            "snd a",
            "set a b",
            "jgz 1 3",
            "snd b",
            "set f 1",
            "add i -1",
            "jgz i -11",
            "snd a",
            "jgz f -16",
            "jgz a -19")
    println(execute(program))
}

fun execute(instructions: List<String>): Long {
    val programs = (0..1).map { Program(0, mutableMapOf(Pair("p", it.toLong())), ArrayDeque<Long>(), 0L, true) }

    var currentProgram = 0
    while (programs[0].running || programs[1].running) {
        executeProgram(instructions, programs[currentProgram], programs[1 - currentProgram].queue)
        currentProgram = 1 - currentProgram
    }
    return programs[1].sendCount
}


fun executeProgram(instructions: List<String>, program: Program, queue: Queue<Long>) {
    var didWork = false
    while (program.instructionPointer in 0 until instructions.size) {
        val tokens = instructions[program.instructionPointer.toInt()].split(" ")
        when (tokens[0]) {
            "snd" -> {
                queue.add(getValue(tokens[1], program.registers))
                program.sendCount++
            }
            "set" -> program.registers[tokens[1]] = getValue(tokens[2], program.registers)
            "add" -> program.registers[tokens[1]] = getValue(tokens[1], program.registers) + getValue(tokens[2], program.registers)
            "mul" -> program.registers[tokens[1]] = getValue(tokens[1], program.registers) * getValue(tokens[2], program.registers)
            "mod" -> program.registers[tokens[1]] = getValue(tokens[1], program.registers) % getValue(tokens[2], program.registers)
            "rcv" -> if (program.queue.size > 0) {
                program.registers[tokens[1]] = program.queue.remove()
            } else {
                program.running = didWork
                return
            }
            "jgz" -> if (getValue(tokens[1], program.registers) > 0) program.instructionPointer += getValue(tokens[2], program.registers) - 1
        }
        program.instructionPointer++
        didWork = true
    }
    program.running = false
}

fun getValue(registerOrLiteral: String, registers: Map<String, Long>): Long {
    return try {
        registerOrLiteral.toLong()
    } catch (e: NumberFormatException) {
        registers[registerOrLiteral] ?: 0
    }
}