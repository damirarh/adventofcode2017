package day24a

data class Component(val a: Int, val b: Int)

fun main(args: Array<String>) {
    val sample = arrayOf(
            "0/2",
            "2/2",
            "2/3",
            "3/4",
            "3/5",
            "0/1",
            "10/1",
            "9/10")
    println(findStrongestBridge(sample))

    val input = arrayOf(
            "42/37",
            "28/28",
            "29/25",
            "45/8",
            "35/23",
            "49/20",
            "44/4",
            "15/33",
            "14/19",
            "31/44",
            "39/14",
            "25/17",
            "34/34",
            "38/42",
            "8/42",
            "15/28",
            "0/7",
            "49/12",
            "18/36",
            "45/45",
            "28/7",
            "30/43",
            "23/41",
            "0/35",
            "18/9",
            "3/31",
            "20/31",
            "10/40",
            "0/22",
            "1/23",
            "20/47",
            "38/36",
            "15/8",
            "34/32",
            "30/30",
            "30/44",
            "19/28",
            "46/15",
            "34/50",
            "40/20",
            "27/39",
            "3/14",
            "43/45",
            "50/42",
            "1/33",
            "6/39",
            "46/44",
            "22/35",
            "15/20",
            "43/31",
            "23/23",
            "19/27",
            "47/15",
            "43/43",
            "25/36",
            "26/38",
            "1/10")
    println(findStrongestBridge(input))
}

fun findStrongestBridge(input: Array<String>): Int {
    val components = parse(input)
    return findStrongestBridge(components, 0)
}

fun findStrongestBridge(components: List<Component>, start: Int): Int {
    var max = 0
    for (component in components) {
        if (component.a == start) {
            max = Math.max(max, findStrongestBridge(components.minus(component), component.b) + component.a + component.b)
        } else if (component.b == start) {
            max = Math.max(max, findStrongestBridge(components.minus(component), component.a) + component.a + component.b)
        }
    }
    return max
}

fun parse(input: Array<String>): List<Component> {
    return input.map { it.split("/")}.map { Component(it[0].toInt(), it[1].toInt()) }
}