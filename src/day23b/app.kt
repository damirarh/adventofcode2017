package day23b

fun main(args: Array<String>) {
    println(optimized(57, 57))
    println(optimized(105700, 122700))
}

fun optimized(b: Int, c: Int): Int {
    return (b..c step 17).count { !isPrime(it) }
}

fun isPrime(n: Int): Boolean {
    if (n <= 1) {
        return false
    }
    if (n <= 3) {
        return true
    }
    if (n % 2 == 0 || n % 3 == 0) {
        return false
    }
    var i = 5
    while (i * i <= n) {
        if (n % i == 0 || n % (i + 2) == 0) {
            return false
        }
        i += 6
    }
    return true
}
