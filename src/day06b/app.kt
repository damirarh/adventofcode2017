package day06b

fun main(args: Array<String>) {
    val sample = arrayOf(0, 2, 7, 0)
    println(reallocate(sample))
    val input = arrayOf(14, 0, 15, 12, 11, 11, 3, 5, 1, 6, 8, 4, 9, 1, 8, 4)
    println(reallocate(input))
}

fun reallocate(memory: Array<Int>): Int {
    val previousStates: ArrayList<String> = arrayListOf()
    var count = 0

    while(!previousStates.contains(memory.joinToString())) {
        count++
        previousStates.add(memory.joinToString())
        redistribute(memory)
    }
    return count - previousStates.indexOf(memory.joinToString())
}

fun redistribute(memory: Array<Int>) {
    val maxValue = memory.max() ?: 0
    val maxIndex = memory.indexOf(maxValue)
    memory[maxIndex] = 0
    for (index in  maxIndex + 1 .. maxIndex + maxValue) {
        memory[index % memory.size]++
    }
}