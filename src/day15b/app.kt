package day15b

data class Generator(val value: Long, val factor: Long, val modulo: Int)

fun main(args: Array<String>) {
    println(judge(1056, 65, 8921))
    println(judge(5000000, 65, 8921))
    println(judge(5000000, 516, 190))
}

fun judge(sampleSize: Int, startA: Long, startB: Long): Int {
    var count = 0
    var generatorA = Generator(startA, 16807, 4)
    var generatorB = Generator(startB, 48271, 8)
    for (iteration in 1..sampleSize) {
        generatorA = generateNext(generatorA)
        generatorB = generateNext(generatorB)
        if (generatorA.value % 65536 == generatorB.value % 65536) {
            count++
        }
    }
    return count
}

fun generateNext(current: Generator): Generator {
    var nextValue = current.value
    do {
        nextValue = nextValue * current.factor % 2147483647
    } while (nextValue % current.modulo != 0L)
    return Generator(nextValue, current.factor, current.modulo)
}