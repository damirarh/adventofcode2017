package day13b

data class Layer(val range: Int, var current: Int, var direction: Int)

fun main(args: Array<String>) {
    val sample = listOf("0: 3",
            "1: 2",
            "4: 4",
            "6: 4")
    println(findFirstSafePassage(sample))

    val input = listOf("0: 3",
            "1: 2",
            "2: 5",
            "4: 4",
            "6: 6",
            "8: 4",
            "10: 8",
            "12: 8",
            "14: 6",
            "16: 8",
            "18: 6",
            "20: 6",
            "22: 8",
            "24: 12",
            "26: 12",
            "28: 8",
            "30: 12",
            "32: 12",
            "34: 8",
            "36: 10",
            "38: 9",
            "40: 12",
            "42: 10",
            "44: 12",
            "46: 14",
            "48: 14",
            "50: 12",
            "52: 14",
            "56: 12",
            "58: 12",
            "60: 14",
            "62: 14",
            "64: 12",
            "66: 14",
            "68: 14",
            "70: 14",
            "74: 24",
            "76: 14",
            "80: 18",
            "82: 14",
            "84: 14",
            "90: 14",
            "94: 17")
    println(findFirstSafePassage(input))
}

fun findFirstSafePassage(firewall: List<String>) : Int {
    val layers = parseLayers(firewall)
    var delay = 0
    while (!isPassageSafe(copyFirewall(layers))) {
        delay++
        updateFirewall(layers)
    }
    return delay
}

fun isPassageSafe(layers: Map<Int, Layer>): Boolean {
    for (position in 0..(layers.keys.max()?: 0)) {
        if (layers.containsKey(position)) {
            val layer = layers[position]
            if (layer?.current == 1) {
                return false
            }
        }
        updateFirewall(layers)
    }
    return true
}

fun parseLayers(firewall: List<String>): Map<Int, Layer> {
    return firewall.map { it.split(": ") }
            .associateBy ( { it[0].toInt() }, { Layer(it[1].toInt(), 1, 1) } )
}

fun updateFirewall(layers: Map<Int, Layer>) {
    for (layer in layers.values) {
        if (layer.direction == 1) {
            layer.current++
            if (layer.current > layer.range) {
                layer.current = layer.range - 1
                layer.direction = -1
            }
        } else {
            layer.current--
            if (layer.current < 1) {
                layer.current = 2
                layer.direction = 1
            }
        }
    }
}

fun resetFirewall(layers: Map<Int, Layer>) {
    for (layer in layers.values) {
        layer.current = 1
        layer.direction = 1
    }
}

fun copyFirewall(layers: Map<Int, Layer>): Map<Int, Layer> {
    return layers.toList().associateBy ({ it.first }, { Layer(it.second.range, it.second.current, it.second.direction ) })
}